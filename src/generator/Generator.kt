package generator

import models.AppClass
import utils.*
import java.io.File

object Generator{
    var pathToTarget: String? = null
    val PATH_TO_SOURCE = "res\\imgSource"

    fun startCreateBets(count: Int, pathToTarget: String){
        Generator.pathToTarget = pathToTarget

        val articles = NewsParser.getArticles(count)
        for(i in articles){

            val app = copyClearCode()

            CopyWriter.copyImage(i)
            CopyWriter.copyBetNative(i)
            NameCleaner.clearNames(app)

            copyAllCode(app)
            NameCleaner.clearManifest()
        }
    }

    fun startCreateSlots(count: Int, pathToTarget: String){
        Generator.pathToTarget = pathToTarget
        for(i in 0 until count){

            val app = copyClearCode()

            CopyWriter.copySlotsItem()
            CopyWriter.copySlotsNative()
            NameCleaner.clearNames(app)

            copyAllCode(app)
            NameCleaner.clearManifest()
        }
    }

    private fun copyClearCode() : AppClass {
        val app = AppClass(NameGenerator.getSingleName(), NameGenerator.getPackageName())
        val targetDir = File(pathToTarget +"\\${app.getAppName()}")
        targetDir.mkdirs()

        CopyWriter.copyClearCode(targetDir, app)

        return app
    }

    private fun copyAllCode(appClass: AppClass){
        CopyProject.copyProject(appClass, "${pathToTarget}\\${appClass.getAppName()}")
        CopyWriter.copyGradle()
        CopyWriter.copyStrings()
        CopyWriter.copyModelFiles()
        CopyWriter.copyJunk()
        CopyWriter.copyManifest()
    }

}