package models

data class AppClass(private val name: String, val pack: String){

    fun getPackageName(): String {
        val sp = pack.split(".")
        val sb = StringBuilder()
        for(i in sp.size-1 downTo   0 ){
            sb.append(sp[i])
            sb.append(".")
        }
        sb.append(name)
        return sb.toString()
    }

    fun getPackagePath() = getPackageName().replace(".", "\\")

    fun getAppName() = name.capitalize()
}