package models

import models.Article

data class News (val status: String, val totalResult: Int, val articles: ArrayList<Article>)