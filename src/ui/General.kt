package ui

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.fxml.FXMLLoader.load
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage

import java.io.File

class General : Application() {
    @Throws(Exception::class)
    override fun start(primaryStage: Stage) {
        primaryStage.scene = Scene(load<Parent?>(General::class.java.getResource("ui.fxml")))
        primaryStage.show()
    }

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            launch(General::class.java)
        }
    }
}
