package ui

import generator.Generator
import javafx.fxml.FXML
import javafx.scene.control.Button
import javafx.scene.control.TextField
import javafx.scene.input.MouseEvent
import javafx.scene.text.Text
import javafx.stage.DirectoryChooser
import javafx.stage.Stage


class UI {
    private var stage: Stage? = null
    @FXML
    lateinit var tf_path: TextField
    @FXML
    lateinit var t_result: Text
    @FXML
    lateinit var btn_generate_bets: Button
    @FXML
    lateinit var btn_generate_slots: Button
    @FXML
    lateinit var tf_count: TextField

    private var pathToFolder: String? = null

    private val generator = Generator


    fun init(primaryStage: Stage) {
        this.stage = primaryStage
        create()
    }

    private fun create() {

    }

    fun selectPath(mouseEvent: MouseEvent) {
        val directoryChooser = DirectoryChooser()
        directoryChooser.title = "Choose project"
        val file = directoryChooser.showDialog(stage)
        if (file != null) {
            pathToFolder = file.absolutePath
            tf_path!!.text = pathToFolder
        }
    }

    fun generateBets(mouseEvent: MouseEvent) {
        if (!tf_path!!.text.isEmpty()) {
            t_result!!.text = ""
            generator.startCreateBets(checkCount(), tf_path!!.text)
            done()
        } else {
            t_result!!.text = "Pls select folder"
        }
    }

    fun generateSlots(mouseEvent: MouseEvent) {
        if (!tf_path!!.text.isEmpty()) {
            generator.startCreateSlots(checkCount(), tf_path!!.text)
            done()
        } else {
            t_result!!.text = "Pls select folder"
        }

    }

    private fun checkCount(): Int {
        try {
            return Integer.parseInt(tf_count!!.text)
        } catch (e: NumberFormatException) {
            t_result!!.text = "Pls, input NUMBER"
            return 0
        }

    }

    private fun done() {
        t_result!!.text = "DONE"
    }

}
