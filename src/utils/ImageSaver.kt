package utils

import generator.Generator
import models.Article
import java.io.FileOutputStream
import java.net.HttpURLConnection
import java.net.URL

object ImageSaver {



    fun saveImage(article: Article) : Boolean{
        try {
            val url = URL(article.urlToImage)
            HttpURLConnection.setFollowRedirects(false)
            val con = url.openConnection() as HttpURLConnection
            con.connectTimeout = 10000

            val inputStream = con.inputStream
            article.pathToImage = "${Generator.PATH_TO_SOURCE}/${NameGenerator.getSingleName()}.jpg"
            val os = FileOutputStream(article.pathToImage)

            val b = ByteArray(2048)
            var length: Int = inputStream.read(b)

            while (length != -1) {
                os.write(b, 0, length)
                length = inputStream.read(b)
            }

            inputStream.close()
            os.close()
            return true
        } catch (e: Exception){
            return false
        }
    }


}