package utils

import models.AppClass
import java.io.File
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths


object NameCleaner {
    private var namesPull: HashMap<String, String?>? = null
    private val charset = StandardCharsets.UTF_8
    private var app: AppClass? = null

    fun clearNames(app: AppClass) {
        namesPull = Words.getNamesPull()
        NameCleaner.app = app

        println(namesPull.toString())

        clearLayouts()
        clearDrawables()
        clearClasses()
    }

    fun clearManifest() {
        clearFileValues(File(CopyWriter.getPathToManifest()))
    }

    private fun clearDrawables() {
        for(f in File(CopyWriter.getPathToDrawable()).listFiles()){
            clearFileName(f)
        }
    }

    private fun clearClasses() {
        for(f in File(CopyWriter.getPathToClasses()).listFiles()){
            clearFileValues(f)
            clearFileName(f)

        }
    }

    private fun clearLayouts() {
        for(f in File(CopyWriter.getPathToLayouts()).listFiles()){
            clearFileValues(f)
            clearFileName(f)
        }
    }

    private fun clearFileValues(f: File) {
        val path = Paths.get(f.absolutePath)

        var content = String(Files.readAllBytes(path), charset)

        for(k in namesPull!!.keys){
            if(content.contains(k)){
                var name = namesPull!![k]
                if(name == null){
                    name = NameGenerator.getSingleName()
                    namesPull!!.put(k, name)
                }
                content = content.replace(k, name)
                println("RENAME VALUE $k TO $name in ${f.nameWithoutExtension}")

            }
        }


        if(content.contains("clearCode")){
            content = content.replace("clearCode", app!!.getPackageName())
        }

        Files.write(path, content.toByteArray(charset))
    }

    private fun clearFileName(f: File){
        if(namesPull!!.containsKey(f.nameWithoutExtension)){

            var name = namesPull!![f.nameWithoutExtension]
            if( name == null){
                name = NameGenerator.getSingleName()
                namesPull!![f.nameWithoutExtension] = name
            }
            println("RENAME FILE ${f.nameWithoutExtension} TO $name")
            f.renameTo(File(f.absolutePath.replace(f.nameWithoutExtension, name)))
        }
    }
}