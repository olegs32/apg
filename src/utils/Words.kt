package utils

import java.util.*

object Words {
    private val names = arrayListOf("ACTIVITY_MAIN", "CHECK_INTERNET", "CHECK_SIM_CARD", "FRAGMENT_WEBVIEW_CODE",
            "FRAGMENT_NATIVE", "SLOTS_ADAPTER")
    private val layouts = arrayListOf("LAYOUT_MAIN_ACTIVITY", "LAYOUT_FRAGMENT_NATIVE", "LAYOUT_FRAGMENT_WEBVIEW",
            "ID_FRAME_LAYOUT_CONTENT", "ID_WEB_VIEW", "LAYOUT_FRAGMENT_NATIVE_SECOND", "LAYOUT_FRAGMENT_NATIVE_THIRD", "SLOTS_ITEM")

    private val ids = arrayListOf("ID_FRAME_LAYOUT_CONTENT", "ID_WEB_VIEW", "BACKGROUND_COLOR")

    private val other = arrayListOf("ICON_FIRST", "ICON_SECOND", "ICON_THIRD", "ICON_FOURTH", "SPIN")

    private val randomColors = arrayListOf("#FFEBEE", "#FCE4EC", "#F3E5F5", "#EDE7F6", "#E8EAF6", "#E3F2FD", "#E1F5FE",
            "#E0F7FA", "#E0F2F1", "#E8F5E9", "#F1F8E9", "#F9FBE7", "#FFFDE7", "#FFF8E1", "#FFF3E0", "#FBE9E7", "#EFEBE9",
            "#FAFAFA")

    private fun getWords(): ArrayList<String> {
        val words = ArrayList<String>()
        words.addAll(names)
        words.addAll(layouts)
//        words.addAll(ids)
        words.addAll(other)

        return words
    }

    fun getRandomColor(): String {
        val random = Random()
        return randomColors[random.nextInt(randomColors.size)]
    }
    fun getNamesPull(): HashMap<String, String?>{
        val pull = HashMap<String, String?>()

        for(s in getWords()){
            pull.put(s, null)
        }
        return pull
    }
}
