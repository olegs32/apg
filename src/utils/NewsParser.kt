package utils

import com.google.gson.Gson
import models.Article
import models.News
import org.json.JSONException
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.Reader
import java.net.HttpURLConnection
import java.net.URL
import java.nio.charset.Charset
import java.util.*

object NewsParser{
    private val games = arrayListOf("%D1%84%D1%83%D1%82%D0%B1%D0%BE%D0%BB", "%D0%B1%D0%B0%D1%81%D0%BA%D0%B5%D1%82%D0%B1%D0%BE%D0%BB",
            "%D1%82%D0%B5%D0%BD%D0%BD%D0%B8%D1%81", "%D1%85%D0%BE%D0%BA%D0%BA%D0%B5%D0%B9")
    private val random = Random()

    @Throws(IOException::class, JSONException::class)
    fun getArticles(count: Int): ArrayList<Article> {
        var ownCount = count
        val a = URL(getUrl())
        val articles = ArrayList<Article>()

        HttpURLConnection.setFollowRedirects(false)
        val con = a.openConnection() as HttpURLConnection
        con.connectTimeout = 10000


        val inputStream = a.openStream()
        inputStream.use { inStream ->


            val rd = BufferedReader(InputStreamReader(inStream, Charset.forName("UTF-8")))
            val jsonText = readAll(rd)
            val news = Gson().fromJson(jsonText, News::class.java)

            println(jsonText)

            var trys = 0
            while (ownCount > 0){
                val randomArticle = news.articles[random.nextInt(100)]

                if(ImageSaver.saveImage(randomArticle)){
                    articles.add(randomArticle)
                    ownCount--
                } else {
                    trys++
                    println("$trys failed")
                }
            }
        }

        return articles
    }

    private fun getUrl(): String {
        val game = games[random.nextInt(games.size)]
        return "https://newsapi.org/v2/everything?q=$game&pageSize=100&apiKey=3ae2aaa3ca544cdd8ddda75c5324a07f"
    }

    @Throws(IOException::class)
    private fun readAll(rd: Reader): String {
        val sb = StringBuilder()
        var cp: Int = rd.read()
        while ((cp) != -1) {
            sb.append(cp.toChar())
            cp = rd.read()
        }
        return sb.toString()
    }

}