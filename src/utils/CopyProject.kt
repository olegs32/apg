package utils

import models.AppClass
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import kotlin.collections.HashSet


object CopyProject {
    private val charset = StandardCharsets.UTF_8
    private val applicationIdTemplateProject = "com.project.test.test_project"
    private var app: AppClass? = null
    private val PATH_TO_TEMPLATE_PROJECT = "res\\templateBetApp"

    fun copyProject(app: AppClass, pathToFolder: String){
        CopyProject.app = app

        copyFolder(File(PATH_TO_TEMPLATE_PROJECT), File(pathToFolder))

        val allFilesList = ArrayList<File>()
        getAllFiles(pathToFolder, allFilesList)

        val allFilesToRename = HashSet<File>()
        val filesWithTestProject = HashSet<File>()
        getListFilesToRenameValue(allFilesList, allFilesToRename, filesWithTestProject)

        allFilesToRename.forEach {
            renamePackages(it)
        }

        filesWithTestProject.forEach {
            renameTEST_PROJECT(it)
        }
    }

    @Throws(IOException::class)
    private fun copyFolder(src: File, dest: File) {
        if (src.isDirectory) {
            if (!dest.exists()) {
                dest.mkdir()
                println("Directory copied from "
                        + src + "  to " + dest)
            }

            val files = src.list()

            for (file in files!!) {
                val srcFile = File(src, file)
                val destFile = File(dest, file)
                copyFolder(srcFile, destFile)
            }

        } else {
            val inStream = FileInputStream(src)
            val outStream = FileOutputStream(dest)

            val buffer = ByteArray(1024)

            var length: Int = inStream.read(buffer)
            while (length > 0) {
                outStream.write(buffer, 0, length)
                length = inStream.read(buffer)
            }

            inStream.close()
            outStream.close()
            println("File copied from $src to $dest")
        }
    }

    private fun getListFilesToRenameValue(files: ArrayList<File>, allFilesToRename: HashSet<File>, filesWithTestProject: HashSet<File>) {
        files.forEach {
            val scanner = Scanner(it)
            var lineNum = 0
            while (scanner.hasNextLine()) {
                val line = scanner.nextLine()
                lineNum++
                if (line.contains(applicationIdTemplateProject)){
                    allFilesToRename.add(it)
                    println("Find package in " + it.absolutePath)
                }
                if(line.contains("TEST_PROJECT")){
                    filesWithTestProject.add(it)
                    println("Find TEST_PROJECT in " + it.absolutePath)
                }
            }
        }
    }

    private fun getAllFiles(directoryName: String, files: ArrayList<File>) {
        val directory = File(directoryName)

        val fList = directory.listFiles()
        for (file in fList!!) {
            if (file.isFile) {
                files.add(file)
            } else if (file.isDirectory) {
                getAllFiles(file.absolutePath, files)
            }
        }
    }

    private fun renamePackages(file: File) {
        val path = Paths.get(file.absolutePath)

        val content = String(Files.readAllBytes(path), charset)
        content.replace(applicationIdTemplateProject, app!!.getPackageName())
        Files.write(path, content.toByteArray(charset))

        println("Rename value \"${applicationIdTemplateProject}\" to \"${app!!.getPackageName()}\" in file \"$path\"")

    }

    private fun renameTEST_PROJECT(file: File){
        val path = Paths.get(file.absolutePath)

        val content = String(Files.readAllBytes(path), charset)
        content.replace("TEST_PROJECT", app!!.getAppName())
        Files.write(path, content.toByteArray(charset))

        println("Rename value TEST_PROJECT to \"${app!!.getPackageName()}\" in file \"${app!!.getAppName()}\"")


    }

}