package utils

import java.lang.StringBuilder
import java.util.*

object NameGenerator {
    private val random = Random()

    fun getPackageName(): String{
        val result = StringBuilder()

        for(i in 0.. 2 ){

            result.append(generateName())

            if(i != 2) {
                result.append(".")
            }
        }
        return result.toString()
    }

    fun getSingleName(): String = generateName()

    private fun generateName(): String {
        val sb = StringBuilder()
        for(i in 0.. random.nextInt(3)+3){
            sb.append((random.nextInt(26)+97).toChar())
        }
        return sb.toString()
    }
}