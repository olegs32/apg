package utils

import models.AppClass
import models.Article
import org.apache.commons.io.FileUtils
import java.io.File
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

object CopyWriter {
    private val PATH_TO_SRC = "res"
    private var targetDir: String? = null
    private var appClass: AppClass? = null
    private var insidePath = "\\app\\src\\main"
    private val charset = StandardCharsets.UTF_8
    private val random = Random()


    fun copyClearCode(targetDir: File, appClass: AppClass) {
        CopyWriter.targetDir = targetDir.absolutePath
        CopyWriter.appClass = appClass

        for(file in File("$PATH_TO_SRC\\clearCode").listFiles())
            if(file.name.contains(".txt")) {
                copyAsClass(file)
            } else {
                copyAsLayout(file)
            }
    }

    private fun copyAsLayout(file: File) {
        FileUtils.copyFile(file, File("${getPathToLayouts()}\\${file.name}"))
    }

    private fun copyAsClass(file: File) {
        val newFile = File("${getPathToClasses()}\\" + file.name.replace(".txt", ".kt"))
        FileUtils.copyFile(file, newFile)
    }

    private fun copyAsBetNative(file: File, article: Article) {
        val path = Paths.get(file.absolutePath)
        val pathToNew = "${getPathToLayouts()}\\LAYOUT_FRAGMENT_NATIVE.xml"


        var content = String(Files.readAllBytes(path), charset)
        content = content.replace("IMAGE_NAME", article.imageName!!)
        content = content.replace("RANDOM_TEXT", (article.title + ". " + article.description).replace("\"", ""))
        content = content.replace("BACKGROUND_COLOR", Words.getRandomColor())
        Files.write(Paths.get(pathToNew), content.toByteArray(charset))
    }

    private fun copyAsSlotsNative(file: File){
        val path = Paths.get(file.absolutePath)
        val pathToNew = "${getPathToLayouts()}\\LAYOUT_FRAGMENT_NATIVE.xml"


        var content = String(Files.readAllBytes(path), charset)
        content = content.replace("BACKGROUND_COLOR", Words.getRandomColor())
        Files.write(Paths.get(pathToNew), content.toByteArray(charset))
    }

    fun copyManifest() {
        val path = Paths.get("${PATH_TO_SRC}\\AndroidManifest.xml")
        val pathToNew = getPathToManifest()


        var content = String(Files.readAllBytes(path), charset)
        content = content.replace("DEFAULT_PACKAGE", appClass!!.getPackageName())
        Files.write(Paths.get(pathToNew), content.toByteArray(charset))
    }

    fun copyGradle() {
        val path = Paths.get("${PATH_TO_SRC}\\build.txt")
        val pathToNew = "${targetDir}\\app\\build.gradle"

        var content = String(Files.readAllBytes(path), charset)
        content = content.replace("DEFAULT_PACKAGE", appClass!!.getPackageName())
        Files.write(Paths.get(pathToNew), content.toByteArray(charset))
    }

    fun copyStrings() {
        val pathToNew = "${getPathToSources()}\\strings.xml"

        var content = String(Files.readAllBytes(Paths.get("${PATH_TO_SRC}\\strings.xml")), charset)
        content = content.replace("DEFAULT_NAME", appClass!!.getAppName())
        Files.write(Paths.get(pathToNew), content.toByteArray(charset))
    }

    fun copyModelFiles(){
        val charset = StandardCharsets.UTF_8

        var path = Paths.get("${PATH_TO_SRC}\\modules.xml")
        var pathToNew = "${targetDir}\\.idea\\modules.xml"
        var content = String(Files.readAllBytes(path), charset)
        content = content.replace("TEST_PROJECT", appClass!!.getAppName())
        Files.write(Paths.get(pathToNew), content.toByteArray(charset))

        path = Paths.get("${PATH_TO_SRC}\\workspace.xml")
        pathToNew = "${targetDir}\\.idea\\workspace.xml"
        content = String(Files.readAllBytes(path), charset)
        content = content.replace("TEST_PROJECT", appClass!!.getAppName())
        Files.write(Paths.get(pathToNew), content.toByteArray(charset))

        path = Paths.get("${PATH_TO_SRC}\\TEST_PROJECT.iml")
        pathToNew = "${targetDir}\\${appClass!!.getAppName()}.iml"
        content = String(Files.readAllBytes(path), charset)
        content = content.replace("TEST_PROJECT", appClass!!.getAppName())
        Files.write(Paths.get(pathToNew), content.toByteArray(charset))
    }

    fun copyImage(article: Article) {
        val imageName = NameGenerator.getSingleName().toLowerCase()

        val fileDest = File("${getPathToDrawable()}\\$imageName.jpg")

        FileUtils.copyFile(File(article.pathToImage), fileDest)
        File(article.pathToImage).delete()
        article.imageName = imageName
    }

    fun copyJunk() {
        val junkClasses = File("${PATH_TO_SRC}\\junkCode").listFiles()
        for( i in 0.. random.nextInt(3)+4){
            val file = junkClasses[random.nextInt(junkClasses.size)]
            val newName = NameGenerator.getSingleName()
            val pathToDest = Paths.get("${getPathToClasses()}\\$newName.java")

            var content = String(Files.readAllBytes(Paths.get(file.absolutePath)), charset)
            content = content.replace(file.nameWithoutExtension, newName)
            content = content.replace("junkCode", appClass!!.getPackageName())
            Files.write(pathToDest, content.toByteArray(charset))
        }
    }

    fun copyBetNative(article: Article) {
        val files = File("${PATH_TO_SRC}\\betNative").listFiles()
        val natives = ArrayList<File>()
        for(file in files)
            if(file.name.contains(".txt")) {
                copyAsClass(file)
            } else {
                natives.add(file)
            }
        copyAsBetNative(natives[Random().nextInt(natives.size)], article)
    }

    fun copySlotsNative() {
        val target = File("${PATH_TO_SRC}\\slots\\slotsNative").listFiles()
        for(file in target)
            when {
                file.name.contains(".txt") -> copyAsClass(file)
                file.name.contains("FRAGMENT_NATIVE") -> copyAsSlotsNative(file)
                else -> copyAsLayout(file)
            }
    }

    private val slotsName = arrayListOf("ICON_FIRST", "ICON_SECOND", "ICON_THIRD", "ICON_FOURTH")

    fun copySlotsItem() {
        val items = File("${PATH_TO_SRC}\\slots\\items").listFiles()
        for(i in 0 until 4){
            val f = items[random.nextInt(items.size)]
            FileUtils.copyFile(f, File("${getPathToDrawable()}\\${slotsName[i]}.png"))
        }

        val spinFile = File("${PATH_TO_SRC}\\slots\\SPIN.png")
        FileUtils.copyFile(spinFile, File("${getPathToDrawable()}\\${spinFile.name}"))
    }



    fun getPathToClasses(): String{
        val file = File("${targetDir}${insidePath}\\java\\${appClass!!.getPackagePath()}")
        file.mkdirs()
        return file.absolutePath
    }

    fun getPathToLayouts(): String{
        val file = File("${targetDir}${insidePath}\\res\\layout")
        file.mkdirs()
        return file.absolutePath
    }

    fun getPathToSources(): String{
        val file = File("${targetDir}${insidePath}\\res\\values")
        file.mkdirs()
        return file.absolutePath
    }

    fun getPathToDrawable(): String{
        val file = File("${targetDir}${insidePath}\\res\\drawable")
        file.mkdirs()
        return file.absolutePath
    }

    fun getPathToManifest() = "${targetDir}${insidePath}\\AndroidManifest.xml"
}